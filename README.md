# Phylo Game Making Workshop
This is a script created for [Hackers and Designer's Summer Academy 2021](https://www.hackersanddesigners.nl/s/Summer_Academy_2021). It will guide you through running your own workshop to create a localized game.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents** 

- [What is Phylo?](#what-is-phylo?)
- [What We'll be Building](#what-we'll-be-building)
- [Materials](#Materials)
- [Workshop Facilitator Preparation](#workshop-facilitator-preparation)
  - [Location of Workshop](#location-of-workshop)
  - [What you need to know about making the deck](#what-you-need-to-know-about-making-the-deck)
  - [More information](#more-information)
- [Workshop Script](#workshop-script)
  - [Day 1](#day-1)
  - [Workshop Facilitator Task](#workshop-facilitator-task)
  - [Day 2](#day-2)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What is Phylo?

Watch [this video](https://youtu.be/dinkNLkmu60) to learn what it is and how to play the game. Here is the [rule page](https://thinkcolorful.org/wp-content/uploads/2020/11/phylo_rules_v6.pdf)

## What We'll be Building

The goal is to create a Phylo card game for your local community featuring animals, plants and events that are present in the community. Using the existing rules and game structure, the workshop will involve gathering field data about plants and animals to integrate into the deck. 

Once all the data is collected to create a deck, the data can be put into the [deck generator app](https://thinkcolorful.org/docgenphylo/phylo_main.html). The participants export their cards and send it to the mediator of the workshop who will use it to create a final deck (using the generator) to be played by all participants. It should take 2 days to complete.

## Materials

> **Materials Needed for everyone:**
> 
> - Computer with internet connection 💻🌐
> - Paper and pencil/pen (or something to take notes outside) 📝
> - Phone with camera 📱
> 
> **Materials for Facilitator:**
> - Printer 🖨
> - Paper (preferably 65lb or lighter cardstock) 📄
> - Scissors or paper cutter ✂
> - Glue (optional for backs or you can double side print backs) 






## Workshop Facilitator Preparation

### Location of Workshop
This workshop can be run in person or virtually. For live in person sessions it's recommended to do it somewhere spacious and close to the outdoors for the outdoor portion of the activity. For a virtual workshop [meet.jit.si](https://meet.jit.si/) and [etherpad](https://etherpad.wikimedia.org/) are recommended for collaborative tools.

### What you need to know about making the deck
**TLDR:** Use the example deck below and look at the example cards in the [deck generator app](https://thinkcolorful.org/docgenphylo/phylo_main.html) (by clicking 'Import' and in the Sample Tab, click Import) for ideas. 

 For an average length game (30 minutes to 1 hour) we recommend that a deck should not have more than 60 cards total. This includes Species, Event and Action cards. As the game is centered around building the ecosystem, there should be about 40 Species cards (70%) and 20 Event and Action cards (30%). This allows events to happen often enough to keep the challenge constant and evolving, without completely overwhelming the players. 

Of the Species cards, the food chain needs to be balanced so that the players have a decent chance to build a sustainable ecosystem. That means that plants (Photosynthetic species) should make up at least 25% of the Species cards, along with Herbivores at 20-25% and Omnivores at about 15-20%. Carnivores have the most specific diet requirements, and as such, they shouldn’t be very plentiful (15-18% maximum). Carnivores also have the Scale requirement, so it is very important that plenty of animals smaller than your carnivores exist in your deck. If the carnivores don’t have enough prey in the deck, the players will never be able to use them in the game!

 For the Events and Actions, typically 1 Action for every 3 or 4 Events provides a good balance of negative and positive cards for the players. Tips for creating action and event cards: Create/brainstorm event cards first using an ecological and game play heuristics, that is think about how these local events affect the real life ecosystem and then how it would affect the game play. Once events cards are decided on, action cards serve as remedies to the event cards. Create actions that would be feesable for people/organizations/governments to do.  


> **Example deck to Use:**
> - 41 Species cards (70%)
>     - 13 Photosynthetic (32% of Species)
>     - 10 Herbivore (25% of Species)
>     - 9 Omnivore (22% of Species)
>     - 6 Carnivore (14% of Species)
>     - 3 Special (7% of Species)
> - 18 Event & Action cards (30%)
>     - 14 Events
>     - 4 Actions
> - 1 Home Card

### Managing Time
I'd suggest splitting the card making evenly in the group. For example if the group has 16 participants, people can be split into teams of 2's and asked to make around 8 cards per team. I'd highly suggest following the example deck above to figure out which cards and how many of each need to be made. 

The climate for the cards should all be the same since the species will be taken from the local environment. Ask participants to only choose 3-4 different types of terrains to ensure species compatibility.

⏱️ If there is a time crunch, the number of cards can be halved to produce a deck that is 30 cards (keeping the same percentage for each type of card, see above).  It is suggested that the number of players get reduced to 2. 

A sample breakdown of cards per person/team/group (this is more specialized):
- Team 1: Make 8 Photosynthetic 
- Team 2: Make 5 Photosynthetic, 3 Herbivore
- Team 3: Make 6 Herbivore, 2 Omnivore
- Team 4: Make 7 Omnivore, 1 Special
- Team 5: Make 6 Carnivore, 2 Special
- Team 6: Make 5 Events, 2 Actions
- Team 7: Make 5 Events, 1 Action
- Team 8: Make 4 Events, 1 Action
- Facilitator: make 1 home card

Another example breakdown of cards per person/team/group (this is more generalized, but also prone to more duplicates):
- Team 1: Make 1 of each type of card 
- Team 2: Make 1 of each type of card 
- Team 3: Make 1 of each type of card 
- Team 4: Make 1 of each type of card except Special, instead make 2 extra Photosynthetic
- Team 5: Make 2 Photosynthetic, 1 Herbivore, 2 Omnivore, 2 Events 
- Team 6: Make 2 Photosynthetic, 1 Herbivore, 1 Omnivore, 1 Carnivore, 2 Events 
- Team 7: Make 1 Photosynthetic, 2 Herbivore, 1 Omnivore, 1 Carnivore, 2 Events 
- Team 8: Make 1 Photosynthetic, 2 Herbivore, 1 Omnivore, 4 Events 
- Facilitator: make 1 home card

💡 If the participants need more ideas, besides the example deck in the card generator there are examples of different types of species cards on the original [phylo.org website ](https://phylogame.org/cards/) and [event cards](https://phylogame.org/classification/event/).



### More information

A more detailed guide can be found [here](https://thinkcolorful.org/?p=1048).

----

## Workshop Script
### Day 1
#### Introduction
Introduce the game and show the break down of cards (like the one above) that are needed to create it. 
Discuss the [deck generator](https://thinkcolorful.org/docgenphylo/phylo_main.html) using the example deck to explain what data is needed and how to input the data.

#### Encourage collaboration
Have a white board or (if doing this virtually) share an ether pad and come up with species and events/actions people would like to see in their decks (try to get them to think about what species they see day to day). This list is to be used as a starting off point, not a final product. Depending on the number of participants they will be assigned to create a certain number of cards and card types (see example deck above and break down who is making what card!). 

#### Go Outside
Invite participants to go outside and find the plants and animals discussed or find new ones. They can take notes on paper or the phone to figure out which animals and plants they want for their assigned cards.
Ask them to take pictures if possible of the species they find. They can also get pictures from Wikipedia Commons or other freely licensed art.  

#### Create Cards

Have participants enter the card data they have into the [deck generator](https://thinkcolorful.org/docgenphylo/phylo_main.html) and export it to JSON and send it to the the workshop facilitator.

---

### Workshop Facilitator Task
1. Once all the cards are in, the facilitator can import all the JSON files. 
1. Double check that the cards look correct. Make sure there are no duplicates, if so remove the duplicate card and add another in if there is time.
1. Export a copy of the full deck (so as to be able to share your deck with the rest of the world)
1. Print them out (see [how to print section](https://thinkcolorful.org/?p=1003))
1. Cut them using scissors or a paper cutter. 
1. Export the final cards to JSON using the deck app and send this to thinkcolorful@thinkcolorful.org so a record can be kept.

> Depending on the number of participants there might be a need for multiple decks. The recommended number of people to play using one deck is up to 4 people. A deck can be created per person, but this would be more labor intensive and it then might be more ideal to have participants print out and cut their own decks. Alternatively one deck can be printed and shared amongst the group.


----
## Day 2
### Play the Game
Review the [video](https://youtu.be/dinkNLkmu60)/[rule page](https://thinkcolorful.org/wp-content/uploads/2020/11/phylo_rules_v6.pdf) and play the game. Encourage participants to think about what worked for the games and what could use improvement ( Perhaps certain species or event/action cards were too powerful? ). 

#### Game Scoring
Game scoring is based on the percentage of maximum points of the deck 
1. Sum up all species card point value = maximum value
1. Calculate the achievement levels based on this maximum value 

- Flourishing ( 85% or higher ) – Your ecosystem is filled with diverse organisms and bristling with growth and life!
- Sustainable ( at least 60% ) – This ecosystem is strong and can serve as a habitat for many different species!
- Damaged ( at least 40% ) – Some organisms may struggle or go extinct in this environment. Keep trying to repair and strengthen it!
- Barren ( 40% or lower ) – This environment is not suitable for most lifeforms. Significant time and work will be needed to bring
growth back to this area.

